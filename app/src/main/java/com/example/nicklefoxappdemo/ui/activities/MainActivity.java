package com.example.nicklefoxappdemo.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.nicklefoxappdemo.R;
import com.example.nicklefoxappdemo.adapter.NewsAdapter;
import com.example.nicklefoxappdemo.constants.Constants;
import com.example.nicklefoxappdemo.retrofitnetworkapi.models.response.Articles;
import com.example.nicklefoxappdemo.retrofitnetworkapi.models.response.ArticlesResponseData;
import com.example.nicklefoxappdemo.viewmodel.NewsViewModel;
import com.google.gson.Gson;


public class MainActivity extends AppCompatActivity implements NewsAdapter.INewsClick{
    private static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private NewsAdapter newsAdapter;
    private  NewsViewModel viewModel;
    private Observer<ArticlesResponseData> observer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setAdapter();
         viewModel = new ViewModelProvider(this).get(NewsViewModel.class);
       observer=new Observer<ArticlesResponseData>() {
             @Override
             public void onChanged(ArticlesResponseData data) {
                 Log.i(TAG, "onChanged " );
                 if (data != null) {
                     Gson gson = new Gson();
                     Log.i(TAG, "Response is: " + gson.toJson(data));

                     if (data.getApiError() != null) {

// All possible error messages are set in retrofit factory class

                         Toast.makeText(MainActivity.this, data.getApiError().message() == null ? getString(R.string.something_went_wrong) : data.getApiError().message(), Toast.LENGTH_SHORT).show();

                     } else if (data.getArticlesList() != null && data.getArticlesList().size() > 0) {
                         newsAdapter.setData(data.getArticlesList());
                     }
                 } else {
                     Toast.makeText(MainActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                 }
                 setProgressBar(false);
             }
         };
         viewModel.getArticles("").observe(this,observer);
        /*viewModel.getArticles("", Constants.API_KEY).observe(this, new Observer<ArticlesResponseData>() {
            @Override
            public void onChanged(ArticlesResponseData data) {
                Log.i(TAG, "onChanged " );
                if (data != null) {
                    Gson gson = new Gson();
                    Log.i(TAG, "Response is: " + gson.toJson(data));

                    if (data.getApiError() != null) {

                        Toast.makeText(MainActivity.this, data.getApiError().message() == null ? "Something went wrong" : data.getApiError().message(), Toast.LENGTH_SHORT).show();

                    } else if (data.getArticlesList() != null && data.getArticlesList().size() > 0) {
                        newsAdapter.setData(data.getArticlesList());
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Something went wrong....", Toast.LENGTH_SHORT).show();
                }
                setProgressBar(false);
            }
        });*/


    }

    private void initViews() {
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void setAdapter() {
        newsAdapter = new NewsAdapter(MainActivity.this);
        newsAdapter.setCallback(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(newsAdapter);
    }

    private void setProgressBar(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
    @Override
    // Initialize the contents of the Activity's options menu
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the Options Menu we specified in XML
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    // This method is called whenever an item in the options menu is selected.
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_all:
                setProgressBar(true);
                viewModel.getArticles("").observe(this,observer);;
              break;
            case R.id.action_eng:
                setProgressBar(true);
                viewModel.getArticles(Constants.ALL_LANG_ENGLISH).observe(this,observer);;
                break;
            case R.id.action_french:
                setProgressBar(true);
                viewModel.getArticles(Constants.ALL_LANG_FRENCH).observe(this,observer);;
                break;
            case R.id.action_spanish:
                setProgressBar(true);
                viewModel.getArticles(Constants.ALL_LANG_SPANISH).observe(this,observer);;
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(Articles articles) {
        if (articles!=null){
            startActivity(WebviewActivity.setIntent(MainActivity.this,
                    articles.getUrl()));
        }
    }
}