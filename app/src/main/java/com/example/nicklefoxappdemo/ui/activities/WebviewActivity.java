package com.example.nicklefoxappdemo.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.nicklefoxappdemo.R;

public class WebviewActivity extends AppCompatActivity {
    private WebView webview;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initViews();

        clear();
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WebviewActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                Toast.makeText(WebviewActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }
        });
        webview.loadUrl(getUrl());
    }

    private void initViews() {
        progressBar=findViewById(R.id.progressBar);
        webview=findViewById(R.id.webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void clear(){
        webview.clearHistory();
        webview.clearFormData();
        webview.clearCache(true);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    }
    public static Intent setIntent(Context pContext, String pURL) {
        Intent intent = new Intent(pContext, WebviewActivity.class);
        intent.putExtra("pURL", pURL);
        return intent;
    }
    private String getUrl(){
        if (getIntent()!=null){
            if (getIntent().getStringExtra("pURL")!=null){
                return getIntent().getStringExtra("pURL");
            }
        }
        return "";
    }
}