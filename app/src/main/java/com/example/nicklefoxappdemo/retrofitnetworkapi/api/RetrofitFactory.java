package com.example.nicklefoxappdemo.retrofitnetworkapi.api;

import com.example.nicklefoxappdemo.retrofitnetworkapi.error.APIError;
import com.example.nicklefoxappdemo.retrofitnetworkapi.error.ErrorUtils;
import com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces.IRetrofitContract;
import com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces.IRetrofitPresenter;
import com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces.IRetrofitResponseCallback;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory<T> implements IRetrofitPresenter<T> {
    private static RetrofitFactory mInstance;
    private IRetrofitResponseCallback mRetrofitCallback;
    public final String TAG = RetrofitFactory.class.getSimpleName();
    private static Retrofit mRetrofitBuilder;
    private int REQUEST_CODE = -1;
    private IRetrofitContract iRetrofitContract;
    private boolean BUILDER_RECREATION = false;

    private RetrofitFactory() {
        super();
    }

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {

                    Request request = chain.request();
                    okhttp3.Response response = chain.proceed(request);
                    System.out.print("Retrofit Response : " + response);


                    return response;
                }
            })
            .build();

    public static synchronized RetrofitFactory getInstance() {
        /*  if (mInstance == null)*/
        mInstance = new RetrofitFactory();


        return mInstance;
    }



    @Override
    public Retrofit getRetrofitBuilder(String baseUrl) {
        if (mRetrofitBuilder == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            mRetrofitBuilder = new retrofit2.Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
//                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    // .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return mRetrofitBuilder;
    }

    @Override
    public void makeRetrofitBuilderNullWhenEnvChanged() {
        mRetrofitBuilder = null;
    }

    @Override
    public IRetrofitContract getRetrofitContract(String baseUrl) {
        //iRetrofitContract =  getRetrofitBuilder(RetroUtils.BASE_URL_L).create(IRetrofitContract.class);
        iRetrofitContract = getRetrofitBuilder(baseUrl).create(IRetrofitContract.class);
        return iRetrofitContract;
    }

    @Override
    public void executeRequest(Call call, int request_code) {
        REQUEST_CODE = request_code;
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response != null && response.isSuccessful()) {
                    System.out.print("Retrofit response: " + response.body());
                    mRetrofitCallback.onResponseReceived(REQUEST_CODE, response.body());
                } else {
                    APIError error = new APIError();
                    error.setStatusCode(-1);
                    if (response.errorBody() != null) {
                        final ResponseBody errorBody = response.errorBody();
                        final String errorMessage;
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            errorMessage = jObjError.optString("message");
                            error.setMessage(errorMessage);
                        } catch (Exception e) {
                            error.setMessage("Some Exception at our end!");
                        }
                    } else {
                        error.setMessage("Unknown Error!");
                    }
                        /*      // … and use it to show error information
                    System.out.print("error message : " + error.message());*/
                    // TODO parse error & navigate user readable error to respective implementation class like {@SignUpPresenterImpl.class}
                    mRetrofitCallback.onErrorReceived(error);


                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                if (t instanceof HttpException) {
                    APIError error = new APIError();
                    error.setStatusCode(2);

                    switch (((HttpException) t).code()) {
                        case HttpsURLConnection.HTTP_UNAUTHORIZED:
                            error.setMessage("Unauthorised User ");
                            break;
                        case HttpsURLConnection.HTTP_FORBIDDEN:
                            error.setMessage("Forbidden");
                            break;
                        case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                            error.setMessage("Internal Server Error");
                            break;
                        case HttpsURLConnection.HTTP_BAD_REQUEST:
                            error.setMessage("Bad Request");
                            break;
                        default:
                            error.setMessage("Unknown Server Error");


                            mRetrofitCallback.onErrorReceived(error);
                    }
                } else {
                    t.printStackTrace();
                    System.out.print("Retrofit mismatch " + t.getMessage());
                    System.out.print("Retrofit mismatch " + t.getLocalizedMessage());
                    System.out.print("Retrofit onFailure()");
                    if (call.isCanceled()) {
                        System.out.print("Retrofit request was cancelled");
                    }

                /*try {
                    RetrofitErrorParse errorBundle = RetrofitErrorParse.adapt(t);
                    assertThat(errorBundle.getAppMessage(), is("Authorization exception"));
                } finally {
                    latch.countDown();
                }*/

                    APIError error = new APIError();
                    error.setStatusCode(-1);
                    error.setMessage(t.getMessage());
                    // TODO parse error & navigate user readable error to respective implementation class like {@SignUpPresenterImpl.class}
                    mRetrofitCallback.onErrorReceived(error);

                }

            }
        });
    }

    @Override
    public void cancelInProcessRequestsByTag(String tag) {
        // TODO Here we will cancel a request by its request tag
    }

    @Override
    public void setRetrofitCallback(IRetrofitResponseCallback callback) {
        mRetrofitCallback = callback;
    }
}
