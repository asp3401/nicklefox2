package com.example.nicklefoxappdemo.retrofitnetworkapi.models.response;

import com.example.nicklefoxappdemo.retrofitnetworkapi.error.APIError;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ArticlesResponseData implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("totalResults")
    private int totalResults;
    @SerializedName("articles")
    private ArrayList<Articles> articlesList;
    private APIError apiError;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public ArrayList<Articles> getArticlesList() {
        return articlesList;
    }

    public void setArticlesList(ArrayList<Articles> articlesList) {
        this.articlesList = articlesList;
    }


    public APIError getApiError() {
        return apiError;
    }


    public void setAPIError(APIError apiError) {
        this.apiError = apiError;
    }
}
