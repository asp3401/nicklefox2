package com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces;

import com.example.nicklefoxappdemo.retrofitnetworkapi.models.response.ArticlesResponseData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IRetrofitContract {
    @GET("everything")
    Call<ArticlesResponseData> getArticles(@Query("language") String language,@Query("qInTitle") String title, @Query("apiKey") String apiKey);
}
