package com.example.nicklefoxappdemo.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class StaticUtil {
    public static String getTime(String dateFormat) {
        String time = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            // use UTC as timezone
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(dateFormat);
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-M-yyyy h:mm:ss a", Locale.ENGLISH);
            outputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            time = outputFormat.format(date);
            return time;

        } catch (Exception e) {
            return time;
        }}
}
