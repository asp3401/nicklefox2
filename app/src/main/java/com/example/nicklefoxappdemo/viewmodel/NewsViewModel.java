package com.example.nicklefoxappdemo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nicklefoxappdemo.constants.Constants;
import com.example.nicklefoxappdemo.repository.NewsRepository;
import com.example.nicklefoxappdemo.retrofitnetworkapi.api.RetrofitFactory;
import com.example.nicklefoxappdemo.retrofitnetworkapi.error.APIError;
import com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces.IRetrofitContract;
import com.example.nicklefoxappdemo.retrofitnetworkapi.interfaces.IRetrofitResponseCallback;
import com.example.nicklefoxappdemo.retrofitnetworkapi.models.response.ArticlesResponseData;
import com.example.nicklefoxappdemo.retrofitnetworkapi.utils.RequestUtil;
import com.example.nicklefoxappdemo.retrofitnetworkapi.utils.RetroUtils;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;


public class NewsViewModel  extends AndroidViewModel {
    // private NewsRepository newsRepository;
    private MutableLiveData<ArticlesResponseData> mutableLiveData;
    private RetrofitFactory<ArticlesResponseData> mRetrofitFactory;

    public NewsViewModel(@NonNull Application application) {
        super(application);
    }


    public LiveData<ArticlesResponseData> getArticles(String language) {
        mutableLiveData = new MutableLiveData<>();
        mRetrofitFactory = RetrofitFactory.getInstance();
        mRetrofitFactory.setRetrofitCallback(new IRetrofitResponseCallback() {
            @Override
            public void onResponseReceived(int REQUEST_CODE, Object object) {
                mutableLiveData.setValue((ArticlesResponseData) object);
            }

            @Override
            public void onErrorReceived(APIError apiError) {
                ArticlesResponseData responseData = new ArticlesResponseData();
                responseData.setAPIError(apiError);
                mutableLiveData.setValue(responseData);
            }
        });

        IRetrofitContract iRetrofitContract = mRetrofitFactory.getRetrofitContract(RetroUtils.BASE_URL);
        Call<ArticlesResponseData> call = iRetrofitContract.getArticles(language,"covid", Constants.API_KEY);
        mRetrofitFactory.executeRequest(call, RequestUtil.NEWS_API_CODE);

        return mutableLiveData;
    }
}
