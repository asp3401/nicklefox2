package com.example.nicklefoxappdemo.constants;

public interface Constants {
    String API_KEY="626dbf516bd74f5dba183c2b0e610b04";
    String ALL_LANG_KEY="";
    String ALL_LANG_FRENCH="fr";
    String ALL_LANG_SPANISH="es";
    String ALL_LANG_ENGLISH="en";
}
