package com.example.nicklefoxappdemo.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.nicklefoxappdemo.R;
import com.example.nicklefoxappdemo.retrofitnetworkapi.models.response.Articles;
import com.example.nicklefoxappdemo.utils.StaticUtil;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {
    private final Context mContext;
    private List<Articles> list = new ArrayList<>();
    private NewsAdapter.INewsClick callback;

    public NewsAdapter(Context ctx) {
        this.mContext = ctx;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);

        return new NewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        final Articles articles = this.list.get(position);
        holder.txtTitle.setText(articles.getTitle() == null ? "" : articles.getTitle());
        holder.txtDesc.setText(articles.getDescription() == null ? "" : articles.getDescription());
        holder.txtDate.setText(StaticUtil.getTime(articles.getPublishedAt()));
        Glide.with(holder.imgThumb.getContext())
                .load(articles.getUrlToImage())
                .into(holder.imgThumb);
        holder.layLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onClick(articles);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        int layout = R.layout.news_item;

        return layout;
    }


    public void setData(List<Articles> mList) {
        //commonAgreement.
        this.list.clear();
        this.list.addAll(mList);
        notifyFeedAdapter();
    }

    public void setCallback(NewsAdapter.INewsClick iNewsClick) {
        this.callback = iNewsClick;
    }


    public void notifyFeedAdapter() {
        notifyDataSetChanged();
    }


    public interface INewsClick {

        void onClick(Articles articles);

    }


    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtDesc, txtDate;
        private ImageView imgThumb;
        private LinearLayout layLinear;

        private NewsViewHolder(View view) {
            super(view);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtDesc = view.findViewById(R.id.txtDesc);
            imgThumb = view.findViewById(R.id.imgThumb);
            txtDate = view.findViewById(R.id.txtDate);
            layLinear = view.findViewById(R.id.layLinear);
        }
    }
}
